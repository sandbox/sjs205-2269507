<?php
/*
 * The following functions are helper functions for the Sensors_chart platform
 */

/*
 * Function to return a list of chart types available
 */
function sensors_chart_get_types($typeID = NULL) {
  $types = array();
  $types[1] = array(
      'name' => 'Scatter graph',
      'description' => 'Scatter graph of points',
      'type' => 'scatter',
      'plugin' => 'google',
      'list' => '2: Scatter',
      );
  $types[2] = array(
      'name' => 'Bar graph',
      'description' => 'Bar graph of points',
      'type' => 'bar',
      'plugin' => 'google',
      'list' => '2: Bar',
      );
  $types[3] = array(
      'name' => 'Pie graph',
      'description' => 'Pie graph of points',
      'type' => 'pie',
      'plugin' => 'google',
      'list' => '3: Pie',
      );
  $types[4] = array(
      'name' => 'column graph',
      'description' => 'Column graph of points',
      'type' => 'column',
      'plugin' => 'google',
      'list' => '4: Column',
      );
  $types[5] = array(
      'name' => 'Line graph',
      'description' => 'Line graph of points',
      'type' => 'line',
      'plugin' => 'google',
      'list' => '5: Line',
      );
  $types[6] = array(
      'name' => 'Area graph',
      'description' => 'Area graph of points',
      'type' => 'area',
      'plugin' => 'google',
      'list' => '6: Area',
      );

  if ($typeID) {
    return $types[$typeID];
  } else {
    return $types;
  }
}

/*
 * Function to return a list of chart data types available
 */
function sensors_chart_get_chart_datasets_list($index=NULL) {
  $types = array(
      1 => t('Raw'),
      2 => t('MAX'),
      3 => t('MIN'),
      4 => t('AVG'),
      5 => t('SUM'),
      );
  if(isset($index) && isset($types[$index])) return($types[$index]);
  else return($types);
}
/*
 * Function to return a list of date range types available
 */
function sensors_chart_get_date_range_list($index=NULL) {
  $date_ranges = array(
      0 => t('Current Reading'),
      1 => t('Date range'),
      2 => t('Last n hours'),
      3 => t('Last n days'),
      4 => t('All'),
      );
  if(isset($index) && isset($date_ranges[$index])) return($date_ranges[$index]);
  else return($date_ranges);
}
/*
 * Function to return a chart 
 */
function sensors_chart_gen_chart_array($chartID,$chart_data) {
  $chart_db = sensors_chart_get_chart_db($chartID);

  $chart = array(
      '#plugin' => '');

  return($chart);
}

function sensors_chart_gen_chart($chartID, $axisID = NULL) {
  $chart_db_obj = sensors_chart_get_charts_db($chartID);

  $chart_db = get_object_vars($chart_db_obj[intval($chartID)]);

  $axis_db = sensors_chart_get_charts_axis_db($axisID,$chartID,null);

  $chart_type = sensors_chart_get_types($chart_db['typeID']);

  $chart = array(
      '#type' => 'chart',
      '#chart_type' => $chart_type['list'],
      '#title' => t($chart_db['title']), 
      '#plugin' => $chart_type['plugin'],
      '#width' => $chart_db['width'],
      '#height' => $chart_db['height'],
      '#data_labels' => TRUE,
      '#redfrom' => 50,
      '#redto' => 100,
      //      '#legend_position' => 'bottom',
      );

  if(isset($chart_db['date']) && isset($chart_db['end_date'])) {
    // => date range
    $start_date = $chart_db['date'];
    $end_date = $chart_db['end_date'];
  } elseif(isset($chart_db['date']) && !isset($chart_db['end_date'])) {
    // => previous n hours/days/etc..
    $date = strtotime($chart_db['date']);
    if(date("H:i:s",$date) == '00:00:00') {
      //last n days
      $n_hours = intval(date("d",$date))*24;
    } else {
      $n_hours = intval(date("H",$date));
    }
  } else {
    // => current measurement
    $start_date = NULL;
    $end_date = NULL;

  }
  switch($chart_type['type']) {
    case 'pie' :
    case 'gauge' :
      $dataset = array();
      foreach($axis_db as $axis) {
        $chart['#max'] = $axis->max;
        $chart['#min'] = $axis->min;

        if($axis->dataset_type == 1) {
          // raw
          $sensor_data =sensors_get_reading_db($axis->sensorID);
          if($sensor_data != NULL) {
            // Coulor does not currently work for individual elements of a chart
            // see:
            // https://code.google.com/p/google-visualization-api-issues/issues/detail?id=1267
            $dataset[] = array(
                t($axis->title), 
                floatval($sensor_data['measurement']),
                '#color' => $axis->colour,);
          }
        }else{ // MIN, MAX, SUM, AVG measurement
          $sensor_data =  sensors_get_last_nhour_measurements_db($axis->sensorID, $n_hours,
              sensors_chart_get_chart_datasets_list($axis->dataset_type));
          if($sensor_data != NULL) {
            foreach($sensor_data as $meas) {
              $dataset[] = array(
                  t($axis->title),
                  floatval($meas->expression),
                  );
            }
          }
        }
      }
      $chart['pie_' . $chartID] = array(
          '#type' => 'chart_data',
          '#title' => t('test data'),
          '#data' => $dataset, 
          );
      break;
    case 'line' :
    case 'scatter' :
    case 'area' :
    case 'bar' :
    case 'column' :
      foreach($axis_db as $axis) {
        $dataset = array();

        if($axis->dataset_type == 1) {
          // Raw Data
          if(isset($n_hours)) {
            // Last * Housrs readings
            $sensor_data =  
              sensors_get_last_nhour_measurements_db($axis->sensorID, $n_hours);
          } elseif(isset($start_date)) {
            // Date based readings
            $sensor_data =  
              sensors_get_date_range_measurements_db($axis->sensorID, $start_date, $end_date);
          } else {
            // Current Readings
            $sensor_data =sensors_get_reading_db($axis->sensorID);
          }
          $dataset = array();
          if($sensor_data != NULL) {
            foreach($sensor_data as $meas) {
              $dataset[] = array(-((time() - strtotime($meas->date))/(60*60)),
                  floatval($meas->measurement));
            }
          }else{
            $dataset[] = array(0,0);
            $dataset[] = array(-60*$n_hours,0);
          }
        }else{ // MIN, MAX, SUM, AVG measurement

          /**********************
            NEED TO SORT OUT DATE RANGES FOR THESE VALS...
           *********************/

          $sensor_data =  sensors_get_last_nhour_measurements_db($axis->sensorID, $n_hours,
              sensors_chart_get_chart_datasets_list($axis->dataset_type));
          if($sensor_data != NULL) {
            foreach($sensor_data as $meas) {
              $dataset[] = array(-60*$n_hours,
                  floatval($meas->expression));
              $dataset[] = array(0,
                  floatval($meas->expression));
            }
          }
        }
        if($axis->title!=NULL) {
          $title=$axis->title;
        } else {
          $units =htmlspecialchars_decode($axis->units);
          $title=$axis->sensorID . ':'. $axis->name ;
          $title .= '-'. $units . '-';
          $title.=' ' . sensors_chart_get_chart_datasets_list($axis->dataset_type);
        }

        $chart['line_' . $axis->axisID] = array(
            '#type' => 'chart_data',
            '#data' => $dataset, 
            '#title' => $title,
            '#color' => $axis->colour, 
            );
      }
      $chart['xaxis'] = array(
          '#type' => 'chart_xaxis',
          '#axis_type' => 'linear',
          '#max' => 0,
          '#min' => isset($n_hours) ? -$n_hours * 1 : NULL,
          );
      $chart['yaxis'] = array(
          '#type' => 'chart_yaxis',
          '#axis_type' => 'linear',
          '#min' => 0,
          );
      break;
  }
  return(array('#markup' => drupal_render($chart)));
}

function sensors_chart_gen_example_chart($typeID) {
  $chart_type_db = sensors_chart_get_types_db($typeID);
  $chart = array(
      '#type' => 'chart',
      '#chart_type' => $chart_type_db['type'],
      '#title' => t("Example chart showing " . $chart_type_db['type'] 
        . " type, using " . $chart_type_db['plugin'] . " plugin."),
      '#legend_position' => 'bottom',
      );
  if($chart_type_db['type'] == 'pie' || $chart_type_db['type'] == 'gauge') {
    $dataset=array(array('Cars',5),array('Motorcycles',2), array('Trains',6),
        array('Boats',9), array('Planes',1));
  } else {
    $dataset=array(array(0,0), array(1,5),array(2,2), array(3,6), array(4,9), array(10,1));
  }
  $chart['line1'] = array(
      '#type' => 'chart_data',
      '#data' => $dataset,
      '#title' => t('my line 1'),
      '#color' => '#B617E5',
      );
  $chart['xaxis'] = array(
      '#type' => 'chart_xaxis',
      '#axis_type' => 'linear',
      );
  $chart['yaxis'] = array(
      '#type' => 'chart_yaxis',
      '#axis_type' => 'linear',
      );
  return(array('#markup' => drupal_render($chart)));
}

function sensors_chart_format_chart_data($chartID, $chart_data) {

}

