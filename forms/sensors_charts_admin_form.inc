<?php



/**
 * Page to display last weeks daily averages 
 */
function sensors_sensors_admin_form() {
  /* css */
  drupal_add_css(drupal_get_path('module', 'sensors') . '/sensors.css');

  return drupal_get_form('sensors_sensors_my_form');
}

/**
 * This function is called the "form builder". It builds the form.
 * Notice, it takes one argument, the $form_state
 */
function sensors_sensors_admin($form_state) {


  $form_state['values']['deviceID'] = get_deviceID();
  $form_state['values']['sensorID'] = get_sensorID();

  $update = FALSE;
  if($form_state['values']['deviceID'] != NULL) {
    /* UPDATE query */
    $device = sensors_get_device_db($form_state['values']['deviceID']);
  }
  if($form_state['values']['sensorID'] != NULL) {
    /* UPDATE query */
    $update = TRUE;
    $sensor = sensors_get_sensor_db($form_state['values']['sensorID']);
  }

  $devices = get_device_list();
  /* DO TYPE STUFF HERE *?

  /* Start Page */
  
  $form['page_container'] = array(
      '#markup' => '<div id="sensors_container">'
      );
  $form['fieldset_sensors'] = array(
      '#type' => 'fieldset',
      '#prefix' => '<div id="sensors_fieldset">',
      '#suffix' => '</div>',
      '#title' => t('Available Sensors'),
      );

  $form['fieldset_sensors']['sensors'] = sensors_list_block(NULL, NULL, 'admin/config/sensors_sensors');
  $form['fieldset_create'] = array(
      '#type' => 'fieldset',
      '#prefix' => '<div id="sensors_fieldset">',
      '#suffix' => '</div>',
      '#title' => ($update ? t('Update Sensor') : t('Create New Sensor')),
      );
  $form['fieldset_create']['row_container_1'] = array(
      '#markup' => '<div id="sensors_row_1">'
      );

  $form['fieldset_create']['sensor_help'] = array(
      '#markup' => '<p>' .
      ($update 
       ? t('Update sensor details below, once complete, press \'Submit\'.') 
       : t('Enter details of the new sensor to be added below, once complete, press Submit.') 
      ) . '</p>',
      );

  $form['fieldset_create']['row_container_1_end'] = array(
      '#markup' => '</div>'
      );
  $form['fieldset_create']['row_container_2'] = array(
      '#markup' => '<div id="sensors_row_2">'
      );
  $form['fieldset_create']['sensor_name_desc'] = array(
      '#type' => 'fieldset',
      '#prefix' => '<div id="sensors_sensor_name_desc">',
      '#suffix' => '</div>',
      );

  if($update) {
    $form['fieldset_create']['sensor_name_desc']['sensorID'] = array(
        '#type' => 'textfield',
        '#title' => t('sensorID'),
        '#size' => 60, 
        '#maxlength' => 32, 
        '#default_value' => $sensor['sensorID'],
        '#attributes' => array('disabled' => 'TRUE'),
        );
  }
  $form['fieldset_create']['sensor_name_desc']['sensor_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Sensor Name'),
      '#size' => 60, 
      '#maxlength' => 32, 
      '#default_value' => ($update ? $sensor['name'] : NULL ),
      );
  $form['fieldset_create']['sensor_name_desc']['sensor_description'] = array(
      '#type' => 'textfield',
      '#title' => t('Sensor Description'),
      '#size' => 60, 
      '#maxlength' => 256, 
      '#default_value' => ($update ? $sensor['description'] : NULL ),
      );
  $form['fieldset_create']['sensor_name_desc']['sensor_radioID'] = array(
      '#type' => 'textfield',
      '#title' => t('Sensor RadioID'),
      '#size' => 60, 
      '#maxlength' => 32, 
      '#default_value' => ($update ? $sensor['radioID'] : NULL ),
      );
  $form['fieldset_create']['sensor_name_desc']['devices_select_inst'] = array(
      '#markup' => '<p>' . t('Select the device the sensor is connected to:') . '</p>',
      );
  $form['fieldset_create']['sensor_name_desc']['devices'] = 
    devices_list_block(
        (isset($sensor['deviceID']) ? $sensor['deviceID'] : 0),
        'admin/config/sensors_sensors');

  $form['fieldset_create']['sensor_name_desc']['sensor_type_select_inst'] = array(
      '#markup' => '<p>' . t('Selet the type of sensor to be added:') . '</p>',
      );
  $form['fieldset_create']['sensor_name_desc']['sensor_type'] =
    sensor_types_list_block(
        (isset($sensor['typeID']) ? $sensor['typeID'] : 0), 
        'admin/config/sensors_type');

  $form['fieldset_create']['row_container_2_end'] = array(
      '#markup' => '</div>'
      );


  $form['fieldset_create']['submit'] = array(
      '#type' => 'submit',
      '#title' => t('Create Profile'),
      '#value' => 'Submit',
      '#prefix' => '<div class="sensors_submit">',
      '#suffix' => '</div>',
      '#submit' => array('sensors_sensors_admin_my_form_submit'),
      );

  $form['page_container_end'] = array(
      '#markup' => '</div>'
      );
  return $form;
}

function sensors_sensors_admin_my_form_submit($form, &$form_state) {

  if(isset($form_state['values']['sensorID'])) {
    /* UPDATE existing Device */
    if(!(isset($form_state['values']['sensor_name']) &&
          isset($form_state['values']['devices_tableselect']) &&
          isset($form_state['values']['types_tableselect']) &&
          isset($form_state['values']['sensor_description']))) {
      drupal_set_message("You must specify at least 'Sensor Name', 'Sensor Description',"
          . " a 'Sensor Type' and the device the sensor is attched to, to update a device.",
          'warning');
    } else {
      $tmp = sensors_update_sensor_db(
          $form_state['values']['sensorID'],
          $form_state['values']['devices_tableselect'],
          $form_state['values']['sensor_name'],
          $form_state['values']['sensor_description'],
          $form_state['values']['types_tableselect'],
          $form_state['values']['sensor_radioID']);
    }
  } else {
    $tmp = 0;

    if(!(isset($form_state['values']['sensor_name']) &&
          isset($form_state['values']['sensor_radioID']) &&
          isset($form_state['values']['devices_tableselect']) &&
          isset($form_state['values']['types_tableselect']) &&
          isset($form_state['values']['sensor_description']))) {
      drupal_set_message("You must specify at least 'Sensor Name', 'Sensor Description',"
          . " a 'Sensor Type' and the device the sensor is attched to, to add a device.",
          'warning');
    } else {
      $tmp = sensors_insert_sensor_db(
          $form_state['values']['devices_tableselect'],
          $form_state['values']['sensor_name'],
          $form_state['values']['sensor_description'],
          $form_state['values']['types_tableselect'],
          $form_state['values']['sensor_radioID']);
    }
  }
  if($tmp < 0) {
    drupal_set_message(t('Your sensor could not be updated...'), 'warning');
  } else {
    drupal_set_message(t('Your sensor has been updated.'), 'status');
  }
  $form_state['redirect'] = array(
      'admin/config/sensors_sensors',
      array(
        ),	
      );
}

