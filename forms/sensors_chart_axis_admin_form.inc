<?php



/**
 * Page to display last weeks daily averages 
 */
function sensors_chart_axis_form() {
  /* css */
  drupal_add_css(drupal_get_path('module', 'sensors_chart_axis') . '/sensors_chart_axis.css');

  return drupal_get_form('sensors_chart_axis_admin');
}

/**
 * This function is called the "form builder". It builds the form.
 * Notice, it takes one argument, the $form_state
 */
function sensors_chart_axis_admin($form_state) {


  $form_state['values']['deviceID'] = get_deviceID();

  $form_state['values']['chartID'] = (isset($_REQUEST['chartID']) ? $_REQUEST['chartID'] : NULL);
  $form_state['values']['sensorID'] = (isset($_REQUEST['sensorID']) ? $_REQUEST['sensorID'] : NULL);
  drupal_set_message($form_state['values']['sensorID'] 
        . ' ' .$form_state['values']['chartID'] );
  $update = FALSE;
  if($form_state['values']['chartID'] != NULL && $form_state['values']['sensorID'] != NULL) {
    $axis_db_obj = sensors_chart_get_charts_axis_db($form_state['values']['chartID'], 
        $form_state['values']['sensorID']);
    if($axis_db_obj != NULL) {
      var_dump($axis_db_obj);

      $axis = get_object_vars($axis_db_obj[intval($form_state['values']['chartID'])]);
      /* UPDATE query */
      $update = TRUE;
    } 
  }

  /* Start Page */

  $form['page_container'] = array(
      '#markup' => '<div id="sensors_chart_axis_container">'
      );


  $form['fieldset_sensors_chart_axis_axis'] = array(
      '#type' => 'fieldset',
      '#prefix' => '<div id="sensors_chart_axis_type_fieldset">',
      '#suffix' => '</div>',
      '#title' => t('Available Axis'),
      );
  $form['fieldset_sensors_chart_axis_axis']['sensors_chart_axis'] = sensors_chart_axis_list_block(NULL, 'admin/config/sensors_chart_axis');

  $form['fieldset_create'] = array(
      '#type' => 'fieldset',
      '#prefix' => '<div id="sensors_chart_axis_fieldset">',
      '#suffix' => '</div>',
      '#title' => t('Create New Axis'),
      );
  $form['fieldset_create']['row_container_1'] = array(
      '#markup' => '<div id="sensors_chart_axis_row_1">'
      );

  $form['fieldset_create']['sensors_chart_axis_help'] = array(
      '#markup' => '<p>' .
      ($update 
       ? t('Update axis details below, once complete, press \'Submit\'.') 
       : t('Enter details of the new axis to be added below, once complete, press Submit.') 
      ) . '</p>',
      );

  $form['fieldset_create']['row_container_1_end'] = array(
      '#markup' => '</div>'
      );
  $form['fieldset_create']['row_container_2'] = array(
      '#markup' => '<div id="sensors_row_2">'
      );
  $form['fieldset_create']['sensors_chart_axis_name_desc'] = array(
      '#type' => 'fieldset',
      '#prefix' => '<div id="sensors_chart_axis_type_name_desc">',
      '#suffix' => '</div>',
      );

  if($update) {
    $form['fieldset_create']['sensors_chart_axis_name_desc']['example'] = 
      sensors_chart_gen_chart($form_state['values']['chartID']);

    $form['fieldset_create']['sensors_chart_axis_name_desc']['chartID'] = array(
        '#type' => 'textfield',
        '#title' => t('chartID'),
        '#size' => 60, 
        '#maxlength' => 32, 
        '#default_value' => $axis['chartID'],
        '#attributes' => array('disabled' => 'TRUE'),
        );
  }
  $form['fieldset_create']['sensors_chart_axis_name_desc']['fieldset_chart'] = array(
      '#type' => 'fieldset',
      '#prefix' => '<div id="sensors_chart_axis_type_fieldset">',
      '#suffix' => '</div>',
      '#title' => t('Available Charts'),
      );
  $form['fieldset_create']['sensors_chart_axis_name_desc']['fieldset_chart']['sensors_chart_axis'] = sensors_chart_list_block($form_state['values']['chartID'], NULL, 'admin/config/sensors_chart_axis');

  if(isset($form_state['values']['chartID'])) {

    $form['fieldset_create']['sensors_chart_axis_name_desc']['fieldset_sensor'] = array(
        '#type' => 'fieldset',
        '#prefix' => '<div id="sensors_chart_axis_type_fieldset">',
        '#suffix' => '</div>',
        '#title' => t('Available Sensors'),
        );
    $form['fieldset_create']['sensors_chart_axis_name_desc']['fieldset_sensor']['sensor'] =
      sensors_list_block(
          (isset($form_state['values']['deviceID']) ? $form_state['values']['deviceID'] : NULL),
          (isset($form_state['values']['sensorID']) ? $form_state['values']['sensorID'] : NULL), 
          NULL, 'admin/config/sensors_chart_axis&chartID=' . $form_state['values']['chartID']);
  }

  $form['fieldset_create']['sensors_chart_axis_name_desc']['sensors_chart_axis_title'] = array(
      '#type' => 'textfield',
      '#title' => t('Axis Title'),
      '#size' => 60, 
      '#maxlength' => 32, 
      '#default_value' => ($update ? $axis['title'] : NULL ),
      '#required' => TRUE,
      );
  $form['fieldset_create']['sensors_chart_axis_name_desc']['sensors_chart_axis_colour'] = array(
      '#type' => 'textfield',
      '#title' => t('Dataset Colour'),
      '#size' => 60, 
      '#maxlength' => 32, 
      '#default_value' => ($update ? $axis['colour'] : NULL ),
      '#required' => TRUE,
      );
  $form['fieldset_create']['sensors_chart_axis_name_desc']['sensors_chart_axis_min'] = array(
      '#type' => 'textfield',
      '#title' => t('Min Axis Value'),
      '#size' => 60, 
      '#maxlength' => 256, 
      '#default_value' => ($update ? $axis['min'] : 0 ),
      );
  $form['fieldset_create']['sensors_chart_axis_name_desc']['sensors_chart_axis_max'] = array(
      '#type' => 'textfield',
      '#title' => t('Max Axis Value'),
      '#size' => 60, 
      '#maxlength' => 256, 
      '#default_value' => ($update ? $axis['max'] : NULL ),
      );

  $form['fieldset_create']['row_container_2_end'] = array(
      '#markup' => '</div>'
      );


  $form['fieldset_create']['submit'] = array(
      '#type' => 'submit',
      '#title' => t('Create Profile'),
      '#value' => 'Submit',
      '#prefix' => '<div class="sensors_submit">',
      '#suffix' => '</div>',
      '#submit' => array('sensors_chart_axis_type_my_form_submit'),
      '#required' => TRUE,
      );

  $form['page_container_end'] = array(
      '#markup' => '</div>'
      );
  return $form;
}

function sensors_chart_axis_type_my_form_submit($form, &$form_state) {
  drupal_set_message($_REQUEST['sensorID'] 
      . ' ' .$_REQUEST['chartID'] 
      . ' ' .$form_state['values']['sensors_chart_axis_title']
      . ' ' .$form_state['values']['sensors_chart_axis_colour']
      . ' ' .$form_state['values']['sensors_chart_axis_min']) ;
  if(!( isset($_REQUEST['sensorID']) && 
        isset($_REQUEST['chartID']) &&
        isset($form_state['values']['sensors_chart_axis_title']) &&
        isset($form_state['values']['sensors_chart_axis_colour']) &&
        isset($form_state['values']['sensors_chart_axis_min']))) {
    drupal_set_message("You must specify at least 'Sensor', 'Title', 'Colour',"
        . " and 'Min  Axis Value' to add or update an axis .", 'warning');
  } else {
    if(isset($form_state['values']['chartID']) && isset($form_state['values']['sensorID'])) {
      /* UPDATE existing Device */
      $tmp = sensors_chart_update_axis_db(
          $_REQUEST['chartID'],
          $_REQUEST['sensorID'],
          $form_state['values']['sensors_chart_axis_title'],
          $form_state['values']['sensors_chart_axis_colour'],
          $form_state['values']['sensors_chart_axis_max'],
          $form_state['values']['sensors_chart_axis_min']);
      if($tmp < 0) {
        drupal_set_message(t('Your axis could not be updated...'), 'warning');
      } else {
        drupal_set_message(t('Your axis has been updated.'), 'status');
      }
    } else {
      $tmp = 0;

      $tmp = sensors_chart_insert_axis_db(
          $_REQUEST['chartID'],
          $_REQUEST['sensorID'],
          $form_state['values']['sensors_chart_axis_title'],
          $form_state['values']['sensors_chart_axis_colour'],
          $form_state['values']['sensors_chart_axis_max'],
          $form_state['values']['sensors_chart_axis_min']);
      if($tmp < 0) {
        drupal_set_message(t('New axis could not be created...'), 'warning');
      } else {
        drupal_set_message(t('Your new axis has been created.'), 'status');
      }
    }
    $form_state['redirect'] = array(
        'admin/config/sensors_chart_axis',
        array(
          ),	
        );
  }
}

