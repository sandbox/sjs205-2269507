<?php



/**
 * page to display last weeks daily averages 
 */
function sensors_chart_admin_form() {
  /* css */
  drupal_add_css(drupal_get_path('module', 'sensors_chart') . '/sensors_chart.css');

  return drupal_get_form('sensors_chart_admin');
}

/**
 * this function is called the "form builder". it builds the form.
 * notice, it takes one argument, the $form_state
 */
function sensors_chart_admin($form_state) {


  $form_state['values']['deviceID'] = get_deviceid();

  $form_state['values']['chartID'] = (isset($_REQUEST['chartID']) ? $_REQUEST['chartID'] : null);
  $update = false;
  if($form_state['values']['chartID'] != null) {
    /* update query */
    $update = true;
    $chart_db_obj = sensors_chart_get_charts_db($form_state['values']['chartID']);
    $chart = get_object_vars($chart_db_obj[$form_state['values']['chartID']]);
  }
  if(isset($_REQUEST['axisID'])) {
    $form_state['values']['axisID'] = $_REQUEST['axisID'];
  }

  if(isset($chart['date']) && isset($chart['end_date'])) {
    $start_date = $chart['date'];
    $end_date = $chart['end_date'];
    $date_range = 1;
  } elseif(isset($chart['date']) && !isset($chart['end_date'])) {
    $date = strtotime($chart['date']);
    if(date("H:i:s",$date) == '00:00:00') {
      //last n days
      $n_date =date("d",$date);
      $date_range = 3;
    } else {
      $n_date = date("H",$date);
      $date_range = 2;
    }
  } else {
    $date_range = NULL;
    $start_date = NULL;
    $end_date = 4;

  }
  /* start page */

  $form['page_container'] = array(
      '#markup' => '<div id="sensors_chart_container">'
      );
  $form['fieldset_sensors_chart'] = array(
      '#type' => 'fieldset',
      '#prefix' => '<div id="sensors_chart_type_fieldset">',
      '#suffix' => '</div>',
      '#title' => t('available charts'),
      );


  $form['fieldset_sensors_chart']['sensor_chart'] = sensors_chart_list_block($form_state['values']['chartID'], null, 'admin/config/sensors_chart');

  $form['fieldset_create'] = array(
      '#type' => 'fieldset',
      '#prefix' => '<div id="sensors_chart_fieldset">',
      '#suffix' => '</div>',
      '#title' => t('create new chart'),
      );
  if(!isset($form_state['values']['axisID'])) {
    $form['fieldset_create']['row_container_1'] = array(
        '#markup' => '<div id="sensors_chart_row_1">'
        );

    $form['fieldset_create']['sensor_chart_help'] = array(
        '#markup' => '<p>' .
        ($update 
         ? t('update chart details below, once complete, press \'submit\'.') 
         : t('enter details of the new chart to be added below, once complete, press submit.') 
        ) . '</p>',
        );

    $form['fieldset_create']['row_container_1_end'] = array(
        '#markup' => '</div>'
        );
    $form['fieldset_create']['row_container_2'] = array(
        '#markup' => '<div id="sensors_row_2">'
        );
    $form['fieldset_create']['sensor_chart_name_desc'] = array(
        '#type' => 'fieldset',
        '#prefix' => '<div id="sensors_chart_type_name_desc">',
        '#suffix' => '</div>',
        );
  }
  if($update) {
    $form['fieldset_create']['sensor_chart_name_desc']['example'] = 
      sensors_chart_gen_chart($form_state['values']['chartID']);
    $form['fieldset_create']['sensor_chart_name_desc']['chartID'] = array(
        '#type' => (!isset($form_state['values']['axisID']) ? 'textfield' : 'hidden'),
        '#title' => t('chartID'),
        '#size' => 60, 
        '#maxlength' => 32, 
        '#default_value' => $chart['chartID'],
        '#attributes' => array(
          'disabled' => 'true',
          ),
        );
  }
  if(!isset($form_state['values']['axisID'])) {
    $form['fieldset_create']['sensor_chart_name_desc']['sensors_chart_title'] = array(
        '#type' => 'textfield',
        '#title' => t('chart title'),
        '#size' => 60, 
        '#maxlength' => 32, 
        '#default_value' => ($update ? $chart['title'] : null ),
        '#required' => true,
        );
    $form['fieldset_create']['sensor_chart_name_desc']['sensors_chart_description'] = array(
        '#type' => 'textfield',
        '#title' => t('chart description'),
        '#size' => 60, 
        '#maxlength' => 256, 
        '#default_value' => ($update ? $chart['description'] : null ),
        '#required' => true,
        );
    $chart_types_ar = sensors_chart_get_types();
    $chart_type_list = array();
    foreach ($chart_types_ar as $id => $chart_type) {
      $chart_type_list[$id] = $id . ': ' . $chart_type['name'] . ' - '
        . $chart_type['plugin'];
    }
    $form['fieldset_create']['sensor_chart_name_desc']['sensors_chart_type_select'] = array(
        '#type' => 'select',
        '#title' => t('Chart Type'),
        '#maxlength' => 32, 
        '#default_value' => (isset($chart['typeID']) ? $chart['typeID']: NULL ),
        '#options' => $chart_type_list,
        '#required' => TRUE,
        );
    $form['fieldset_create']['sensor_chart_name_desc']['row_dates'] = array(
        '#markup' => '<div id="cc_dates">'
        );

    $form['fieldset_create']['sensor_chart_name_desc']['date_type_fieldset'] = array(
        '#type' => 'fieldset',
        '#prefix' => '<div id="cc_profile_date_range">',
        '#suffix' => '</div>',
        );
    $date_ranges = sensors_chart_get_date_range_list();
    $form['fieldset_create']['sensor_chart_name_desc']['date_type_fieldset']
      ['sensors_chart_daterange_select'] = array(
          '#type' => 'select',
          '#title' => t('Date Range Type'),
          '#maxlength' => 32, 
          '#default_value' => (isset($date_range) ? $date_range : NULL ),
          '#options' => $date_ranges,
          '#required' => TRUE,
          );
    $form['fieldset_create']['sensor_chart_name_desc']['date_type_fieldset']
      ['date_n_value'] = array(
          '#type' => 'textfield',
          '#title' => t('n'),
          '#size' => 60, 
          '#maxlength' => 32, 
          '#default_value' => (isset($n_date) ? $n_date : NULL ),
          '#states' => array(
            'visible' => array(
              array(
                array(':input[name="sensors_chart_daterange_select"]' => array('value' => '2')),
                'xor',
                array(':input[name="sensors_chart_daterange_select"]' => array('value' => '3')),
                ),
              ),
            ),
          );

    $format = 'Y-m-d H:i:s';
    $form['fieldset_create']['sensor_chart_name_desc']['start_date_fieldset'] = array(
        '#type' => 'fieldset',
        '#prefix' => '<div id="cc_profile_start_date">',
        '#suffix' => '</div>',
        '#states' => array(
          'visible' => array(
            ':input[name="sensors_chart_daterange_select"]' => array('value' => '1'),
            ),
          ),
        );


    $form['fieldset_create']['sensor_chart_name_desc']['start_date_fieldset']['chart_date_start'] = array(
        '#type' => 'date_popup',
        '#title' => t('Start Date'),
        '#date_format' => $format,
        '#date_label_position' => 'above',
        '#date_increment' => 1, /*to increment minutes */
        '#date_year_range' => '-25:+25',
        '#default_value' => isset($start_date) ? $start_date : NULL,
        '#prefix' => '<div id="cc_profile_start_date_internal">',
        );
    $form['fieldset_create']['sensor_chart_name_desc']['start_date_fieldset']['end_date_later'] = array(
        '#type' => 'checkbox',
        '#title' => t('Set End Date Later'),
        '#default_value' => TRUE,
        '#suffix' => '</div>',
        );

    $form['fieldset_create']['sensor_chart_name_desc']['end_date_fieldset'] = array(
        '#type' => 'fieldset',
        '#prefix' => '<div id="cc_profile_end_date">',
        '#suffix' => '</div>',
        '#states' => array(
          'visible' => array(
            ':input[name="end_date_later"]' => array('checked' => FALSE)),
          ),
        );
    $form['fieldset_create']['sensor_chart_name_desc']['end_date_fieldset']['end_date'] = array(
        '#type' => 'date_popup',
        '#title' => t('End Date'),
        '#date_format' => $format,
        '#date_label_position' => 'above',
        '#date_increment' => 1, /*to increment minutes */
        '#date_year_range' => '-25:+25',
        '#default_value' => isset($end_date) ? $end_date : NULL,
        );

    $form['fieldset_create']['sensor_chart_name_desc']['sensors_chart_width'] = array(
        '#type' => 'textfield',
        '#title' => t('Chart Width'),
        '#size' => 60, 
        '#maxlength' => 32, 
        '#default_value' => ($update ? $chart['width'] : NULL ),
        '#required' => TRUE,
        );
    $form['fieldset_create']['sensor_chart_name_desc']['sensors_chart_height'] = array(
        '#type' => 'textfield',
        '#title' => t('Chart Height'),
        '#size' => 60, 
        '#maxlength' => 32, 
        '#default_value' => ($update ? $chart['height'] : NULL ),
        '#required' => TRUE,
        );
    $form['fieldset_create']['sensor_chart_name_desc']['create_block_select'] = array(
        '#type' => 'select',
        '#title' => t('Create block for this chart?'),
        '#maxlength' => 32, 
        '#default_value' => (isset($chart['block']) ? $chart['block']: NULL ),
        '#options' => array(0 => 'No', 1 => 'Yes'),
        );
    $form['fieldset_create']['sensor_chart_name_desc']['sensors_chart_priority'] = array(
        '#type' => 'textfield',
        '#title' => t('Priority'),
        '#size' => 60, 
        '#maxlength' => 256, 
        '#default_value' => ($update ? $chart['priority'] : NULL ),
        '#required' => TRUE,
        );
  }

  if($update) { 
    $form['fieldset_create']['sensor_chart_name_desc']['container_axis_block'] = 
      sensors_chart_axis_add_block(
          (isset($_REQUEST['axisID']) ? $_REQUEST['axisID'] : NULL),
          $chart['chartID'], 'admin/config/sensors_chart');
  }



  $form['fieldset_create']['row_container_2_end'] = array(
      '#markup' => '</div>'
      );


  if(!isset($form_state['values']['axisID'])) {
    $form['fieldset_create']['submit'] = array(
        '#type' => 'submit',
        '#title' => t('Create Profile'),
        '#value' => 'Submit',
        '#prefix' => '<div class="sensors_submit">',
        '#suffix' => '</div>',
        '#submit' => array('sensors_chart_type_my_form_submit'),
        '#required' => TRUE,
        );
  }

  $form['page_container_end'] = array(
      '#markup' => '</div>'
      );
  return $form;
}

function sensors_chart_type_my_form_submit($form, &$form_state) {
  $create_block = TRUE;
  //ensure correct date formatting
  $error = FALSE;
  $end_date = NULL;
  $start_date = NULL;
  if(isset($form_state['values']['sensors_chart_daterange_select'])) {
    switch($form_state['values']['sensors_chart_daterange_select']) {
      case '1': //date range
        if(isset($form_state['values']['chart_date_start']) &&
            ($form_state['values']['end_date_later'] == FALSE  ||
             ($form_state['values']['end_date_later']  && 
              isset($form_state['values']['end_date'])))) {

          $start_date = $form_state['values']['chart_date_start'];
          $end_date = $form_state['values']['end_date'];
        }else {
          $error = TRUE;
        }
        break;
      case '2':
        if(isset($form_state['values']['date_n_value'])) {
          $start_date = '1970-01-01 ' . sprintf("%02s",$form_state['values']['date_n_value'])
            . ':00:00';
          $end_date = NULL;
        }else {
          $error = TRUE;
        }

        break;


      case '3':
        if(isset($form_state['values']['date_n_value'])) {
          $start_date = '1970-01-' . sprintf("%02s",$form_state['values']['date_n_value'])
            . ' 00:00:00';
          $end_date = NULL;
        }else {
          $error = TRUE;
        }
        break;

      default:
    }
  }
  if($error && !( isset($form_state['values']['sensors_chart_type_select']) &&
        isset($form_state['values']['sensors_chart_title']) &&
        isset($form_state['values']['sensors_chart_description']) &&
        isset($form_state['values']['sensors_chart_width']) &&
        isset($form_state['values']['sensors_chart_height']) &&
        isset($form_state['values']['sensors_chart_priority']))) {
    drupal_set_message("You must specify at least 'Sensor', 'Chart Type', 'Description',"
        . " 'Width', 'Height' and 'Priority' to update a chart .", 'warning');
  } else {

    if(isset($form_state['values']['chartID'])) {
      /* UPDATE existing Device */
      $tmp = sensors_chart_update_chart_db(
          $form_state['values']['chartID'],
          $form_state['values']['sensors_chart_type_select'],
          $form_state['values']['sensors_chart_title'],
          $form_state['values']['sensors_chart_description'],
          $start_date,
          $end_date,
          $form_state['values']['sensors_chart_width'],
          $form_state['values']['sensors_chart_height'],
          $form_state['values']['create_block_select'],
          $form_state['values']['sensors_chart_priority']);
      if($tmp < 0) {
        drupal_set_message(t('Your chart type could not be updated...'), 'warning');
      } else {
        drupal_set_message(t('Your chart type has been updated.'), 'status');
      }
    } else {
      $tmp = 0;

      $tmp = sensors_chart_insert_chart_db(
          $form_state['values']['sensors_chart_type_select'],
          $form_state['values']['sensors_chart_title'],
          $form_state['values']['sensors_chart_description'],
          $start_date,
          $end_date,
          $form_state['values']['sensors_chart_width'],
          $form_state['values']['sensors_chart_height'],
          $form_state['values']['create_block_select'],
          $form_state['values']['sensors_chart_priority']);
      if($tmp < 0) {
        drupal_set_message(t('New chart could not be created...'), 'warning');
      } else {
        drupal_set_message(t('Your new chart has been created.'), 'status');
      }
    }

    if($create_block) {
    /*  sensors_chart_create_chart_block($form_state['values']['chartID'],
          'test chart block 2','test2' );*/
    }
    $form_state['redirect'] = array(
        'admin/config/sensors_chart',
        array(
          ),	
        );
  }
}
