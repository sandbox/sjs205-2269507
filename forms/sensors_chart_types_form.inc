<?php



/**
 * Page to display last weeks daily averages 
 */
function sensors_chart_types_form() {
  /* css */
  drupal_add_css(drupal_get_path('module', 'sensors_chart') . '/sensors_chart.css');

  return drupal_get_form('sensors_chart_types_my_form');
}

/**
 * This function is called the "form builder". It builds the form.
 * Notice, it takes one argument, the $form_state
 */
function sensors_chart_types_admin($form_state) {


  $form_state['values']['typeID'] = (isset($_REQUEST['typeID']) ? $_REQUEST['typeID'] : NULL);
  $update = FALSE;
  if($form_state['values']['typeID'] != NULL) {
    /* UPDATE query */
    $update = TRUE;
    $chart_type = sensors_chart_get_types($form_state['values']['typeID']);
  }

  /* DO TYPE STUFF HERE *?

  /* Start Page */
  
  $form['page_container'] = array(
      '#markup' => '<div id="sensors_chart_container">'
      );
  $form['fieldset_sensors_chart'] = array(
      '#type' => 'fieldset',
      '#prefix' => '<div id="sensors_chart_type_fieldset">',
      '#suffix' => '</div>',
      '#title' => t('Available Chart Types'),
      );


  $form['fieldset_sensors_chart']['sensor_chart_types'] = sensors_chart_types_list_block(NULL, 'admin/config/sensors_chart_types');


  $form['page_container_end'] = array(
      '#markup' => '</div>'
      );
  return $form;
}

