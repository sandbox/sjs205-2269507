<?php

/**
 * implimentation of hook_views_data()
 */
function sensors_charts_views_data() {

  $data = array();
  /* device table */
/*  $data['sensors_device']['table']['group'] = t('Sensor devices');

  $data['sensors_device']['table']['base'] = array(
      'field' => 'deviceID',
      'title' => t('Sensors Device Data'),
      'help' => t('Sensors device information'),
      );  

  $data['sensors_device']['deviceID'] =  array(
      'title' =>  t('deviceID'),
      'help'  =>  t('Number used to identify devices'),
      'field' =>  array(
        'handler' => 'views_handler_field_numeric', 
        'click sortable' => TRUE,
        ),  
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
        ),  
      'argument' => array(
        'handler' => 'views_handler_argument_numeric',
        ),  
      );  

  $data['sensors_device']['sver'] =  array(
      'title' =>  t('Software Version'),
      'help'  =>  t('Device\'s Software Version'),
      'field' =>  array(
        'handler' => 'views_handler_field', 
        'click sortable' => TRUE,
        ),  
      'sort' => array(
        'handler' => 'views_handler_sort',
        ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
        ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
        ),
      );  

  $data['sensors_device']['master'] =  array(
      'title' =>  t('master'),
      'help'  =>  t('Used to identify a master devices, i.e. main device that should be shown'),
      'field' =>  array(
        'handler' => 'views_handler_field_numeric', 
        'click sortable' => TRUE,
        ),  
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
        ),  
      'argument' => array(
        'handler' => 'views_handler_argument_numeric',
        ),  
      );  
  
  $data['sensors_device']['name'] =  array(
      'title' =>  t('Device Name'),
      'help'  =>  t('Name of device'),
      'field' =>  array(
        'handler' => 'views_handler_field', 
        'click sortable' => TRUE,
        ),  
      'sort' => array(
        'handler' => 'views_handler_sort',
        ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
        ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
        ),
      );  

  $data['sensors_device']['description'] =  array(
      'title' =>  t('Device Description'),
      'help'  =>  t('Short description of device'),
      'field' =>  array(
        'handler' => 'views_handler_field', 
        'click sortable' => TRUE,
        ),  
      'sort' => array(
        'handler' => 'views_handler_sort',
        ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
        ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
        ),
      );  

  $data['sensors_sensor_type']['table']['group'] = t('Sensors sensor types');
  $data['sensors_sensor_type']['table']['join']['sensors_sensor'] = array(
      'left_field' => 'typeID',
      'field' => 'typeID',
      );

  $data['sensors_sensor_type']['typeID'] =  array(
      'title' =>  t('Sensor TypeID'),
      'help'  =>  t('Type of sensor device'),
      'field' =>  array(
        'handler' => 'views_handler_field_numeric',
        'click sortable' => FALSE,
        ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
        ),
      );

  $data['sensors_sensor_type']['units'] =  array(
      'title' =>  t('Sensor Units'),
      'help'  =>  t('Sensor measurement units'),
      'field' =>  array(
        'handler' => 'views_handler_field', 
        'click sortable' => TRUE,
        ),  
      'sort' => array(
        'handler' => 'views_handler_sort',
        ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
        ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
        ),
      );  

  $data['sensors_sensor_type']['name'] =  array(
      'title' =>  t('Sensor Type'),
      'help'  =>  t('Type of sensor'),
      'field' =>  array(
        'handler' => 'views_handler_field', 
        'click sortable' => TRUE,
        ),  
      'sort' => array(
        'handler' => 'views_handler_sort',
        ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
        ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
        ),
      );  

  $data['sensors_sensor_type']['description'] =  array(
      'title' =>  t('Sensor  Description'),
      'help'  =>  t('Short description of sensor type'),
      'field' =>  array(
        'handler' => 'views_handler_field', 
        'click sortable' => TRUE,
        ),  
      'sort' => array(
        'handler' => 'views_handler_sort',
        ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
        ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
        ),
      );  


  $data['sensors_sensor_calc']['table']['group'] = t('Sensors sensor calcs');
  $data['sensors_sensor_calc']['table']['join']['sensors_sensor'] = array(
      'left_field' => 'sensorID',
      'field' => 'sensorID',
      );

  $data['sensors_sensor_calc']['calc_type'] =  array(
      'title' =>  t('Calculation Type'),
      'help'  =>  t('Type of sensor calculation'),
      'field' =>  array(
        'handler' => 'views_handler_field', 
        'click sortable' => TRUE,
        ),  
      'sort' => array(
        'handler' => 'views_handler_sort',
        ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
        ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
        ),
      );  

  $data['sensors_sensor_calc']['date'] =  array(
      'title' =>  t('Date of Calc'),
      'help'  =>  t('The date and time the calculation starts'),
      'field' =>  array(
        'handler' => 'views_handler_field_datetime', 
        'click sortable' => TRUE,
        ),  
      'sort' => array(
        'handler' => 'views_handler_sort_datetime',
        ),
      'filter' => array(
        'handler' => 'views_handler_filter_datetime',
        ),  
      'argument' => array(
        'handler' => 'views_handler_argument_date',
        ),  
      );  
  $data['sensors_sensor_calc']['time_interval'] =  array(
      'title' =>  t('The interval of the Calc'),
      'help'  =>  t('The date and time the calculation ends'),
      'field' =>  array(
        'handler' => 'views_handler_field', 
        'click sortable' => TRUE,
        ),  
      'sort' => array(
        'handler' => 'views_handler_sort',
        ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
        ),  
      'argument' => array(
        'handler' => 'views_handler_argument_string',
        ),  
      );  
  $data['sensors_sensor_calc']['expires'] =  array(
      'title' =>  t('Calc Expires'),
      'help'  =>  t('Date the calculation expires'),
      'field' =>  array(
        'handler' => 'views_handler_field_datetime', 
        'click sortable' => TRUE,
        ),  
      'sort' => array(
        'handler' => 'views_handler_sort_datetime',
        ),
      'filter' => array(
        'handler' => 'views_handler_filter_datetime',
        ),  
      'argument' => array(
        'handler' => 'views_handler_argument_date',
        ),  
      );  
  $data['sensors_sensor_calc']['result'] =  array(
      'title' =>  t('Calc Result'),
      'help'  =>  t('Result of calculation'),
      'field' =>  array(
        'handler' => 'views_handler_field', 
        'click sortable' => TRUE,
        ),  
      'sort' => array(
        'handler' => 'views_handler_sort',
        ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
        ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
        ),
      );  
  $data['sensors_sensor']['table']['group'] = t('Sensors Sensors');
  $data['sensors_sensor']['table']['base'] = array(
      'field' => 'sensorID',
      'title' => t('Sensors Sensor Data'),
      'help' => t('Sensors sensor data and readings'),
      );  

  $data['sensors_sensor']['table']['join']['sensors_device'] = array(
      'left_field' => 'deviceID',
      'field' => 'deviceID',
      );

  $data['sensors_sensor']['sensorID'] =  array(
      'title' =>  t('sensorID'),
      'help'  =>  t('Number used to identify sensor'),
      'field' =>  array(
        'handler' => 'views_handler_field_numeric', 
        'click sortable' => TRUE,
        ),  
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
        ),  
      'argument' => array(
        'handler' => 'views_handler_argument_numeric',
        ),  
      );  
  $data['sensors_sensor']['deviceID'] =  array(
      'title' =>  t('deviceID'),
      'help'  =>  t('Number used to identify devices'),
      'field' =>  array(
        'handler' => 'views_handler_field_numeric', 
        'click sortable' => TRUE,
        ),  
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
        ),  
      'argument' => array(
        'handler' => 'views_handler_argument_numeric',
        ),  
      );  

  $data['sensors_sensor']['radioID'] =  array(
      'title' =>  t('RadioID'),
      'help'  =>  t('Sensor\'s Radio ID'),
      'field' =>  array(
        'handler' => 'views_handler_field', 
        'click sortable' => TRUE,
        ),  
      'sort' => array(
        'handler' => 'views_handler_sort',
        ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
        ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
        ),
      );  

  $data['sensors_sensor']['typeID'] =  array(
      'title' =>  t('Sensor TypeID'),
      'help'  =>  t('Type of sensor device'),
      'field' =>  array(
        'handler' => 'views_handler_field_numeric',
        'click sortable' => FALSE,
        ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
        ),
      );
  $data['sensors_sensor']['master'] =  array(
      'title' =>  t('master'),
      'help'  =>  t('Used to identify a master sensors, i.e. main sensors that should be shown'),
      'field' =>  array(
        'handler' => 'views_handler_field_numeric', 
        'click sortable' => TRUE,
        ),  
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
        ),  
      'argument' => array(
        'handler' => 'views_handler_argument_numeric',
        ),  
      );  

  $data['sensors_sensor']['name'] =  array(
      'title' =>  t('Sensor Name'),
      'help'  =>  t('User\'s name for sensor'),
      'field' =>  array(
        'handler' => 'views_handler_field', 
        'click sortable' => TRUE,
        ),  
      'sort' => array(
        'handler' => 'views_handler_sort',
        ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
        ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
        ),
      );  

  $data['sensors_sensor']['description'] =  array(
      'title' =>  t('Sensor Description'),
      'help'  =>  t('User\'s Short description of device'),
      'field' =>  array(
        'handler' => 'views_handler_field', 
        'click sortable' => TRUE,
        ),  
      'sort' => array(
        'handler' => 'views_handler_sort',
        ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
        ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
        ),
      );  

  $data['sensors_reading']['table']['group'] = t('Sensors Reading');
  $data['sensors_reading']['table']['join']['sensors_device'] = array(
      'left_field' => 'deviceID',
      'field' => 'deviceID',
      );

  $data['sensors_reading']['readingID'] =  array(
      'title' =>  t('readingID'),
      'help'  =>  t('Number used to identify sensor reading'),
      'field' =>  array(
        'handler' => 'views_handler_field_numeric',
        'click sortable' => FALSE,
        ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
        ),
      );


  $data['sensors_reading']['deviceID'] =  array(
      'title' =>  t('deviceID'),
      'help'  =>  t('Number used to identify device reading is attached to'),
      'field' =>  array(
        'handler' => 'views_handler_field_numeric',
        'click sortable' => FALSE,
        ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
        ),
      );
*/
  /* TIME STAMP */
/*
  $data['sensors_reading']['date'] =  array(
      'title' =>  t('Reading Date'),
      'help'  =>  t('Date of the measurement'),
      'field' =>  array(
        'handler' => 'views_handler_field_datetime', 
        'click sortable' => TRUE,
        ),  
      'sort' => array(
        'handler' => 'views_handler_sort_datetime',
        ),
      'filter' => array(
        'handler' => 'views_handler_filter_datetime',
        ),  
      'argument' => array(
        'handler' => 'views_handler_argument_date',
        ),  
      );  

  $data['sensors_measurement']['table']['group'] = t('Sensors Reading');
  $data['sensors_measurement']['table']['join']['sensors_reading'] = array(
      'left_table' => 'sensors_reading',
      'left_field' => 'readingID',
      'field' => 'readingID',
      );
  $data['sensors_measurement']['table']['join']['sensors_sensor'] = array(
      'left_table' => 'sensors_sensor',
      'left_field' => 'sensorID',
      'field' => 'sensorID',
      );

  $data['sensors_measurement']['readingID'] =  array(
      'title' =>  t('readingID'),
      'help'  =>  t('Number used to identify the reading the measurement is attached to'),
      'field' =>  array(
        'handler' => 'views_handler_field_numeric',
        'click sortable' => FALSE,
        ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
        ),
      );
  $data['sensors_measurement']['sensorID'] =  array(
      'title' =>  t('sensorID'),
      'help'  =>  t('Number used to identify sensor measurement is attached to'),
      'field' =>  array(
        'handler' => 'views_handler_field_numeric',
        'click sortable' => FALSE,
        ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
        ),
      );

  $data['sensors_measurement']['name'] =  array(
      'title' =>  t('Measurement Name'),
      'help'  =>  t('Name of measurement'),
      'field' =>  array(
        'handler' => 'views_handler_field', 
        'click sortable' => TRUE,
        ),  
      'sort' => array(
        'handler' => 'views_handler_sort',
        ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
        ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
        ),
      );  

  $data['sensors_measurement']['measurement'] =  array(
      'title' =>  t('Measurement'),
      'help'  =>  t('Reading measurement'),
      'field' =>  array(
        'handler' => 'views_handler_field', 
        'click sortable' => TRUE,
        ),  
      'sort' => array(
        'handler' => 'views_handler_sort',
        ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
        ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
        ),
      );  

  $data['sensors_sensor_profile']['table']['group'] = t('Sensors sensor profiles');
  $data['sensors_sensor_profile']['table']['join']['sensors_sensor'] = array(
      'left_field' => 'sensorID',
      'field' => 'sensorID',
      );
  $data['sensors_sensor_profile']['profileID'] =  array(
      'title' =>  t('profileID'),
      'help'  =>  t('Number used to identify profile'),
      'field' =>  array(
        'handler' => 'views_handler_field_numeric', 
        'click sortable' => TRUE,
        ),  
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
        ),  
      'argument' => array(
        'handler' => 'views_handler_argument_numeric',
        ),  
      );  

  $data['sensors_sensor_profile']['sensorID'] =  array(
      'title' =>  t('sensorID'),
      'help'  =>  t('Number used to identify sensor profile is attached to'),
      'field' =>  array(
        'handler' => 'views_handler_field_numeric',
        'click sortable' => FALSE,
        ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
        ),
      );

  $data['sensors_sensor_profile']['name'] =  array(
      'title' =>  t('Profile Name'),
      'help'  =>  t('Name of profile'),
      'field' =>  array(
        'handler' => 'views_handler_field', 
        'click sortable' => TRUE,
        ),  
      'sort' => array(
        'handler' => 'views_handler_sort',
        ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
        ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
        ),
      );  

  $data['sensors_sensor_profile']['description'] =  array(
      'title' =>  t('Profile Description'),
      'help'  =>  t('Short description of profile'),
      'field' =>  array(
        'handler' => 'views_handler_field', 
        'click sortable' => TRUE,
        ),  
      'sort' => array(
        'handler' => 'views_handler_sort',
        ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
        ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
        ),
      );  
  $data['sensors_sensor_profile']['date'] =  array(
      'title' =>  t('Start Date of Profile'),
      'help'  =>  t('The date and time the profile starts'),
      'field' =>  array(
        'handler' => 'views_handler_field_datetime', 
        'click sortable' => TRUE,
        ),  
      'sort' => array(
        'handler' => 'views_handler_sort_datetime',
        ),
      'filter' => array(
        'handler' => 'views_handler_filter_datetime',
        ),  
      'argument' => array(
        'handler' => 'views_handler_argument_date',
        ),  
      );  
  $data['sensors_sensor_profile']['end_date'] =  array(
      'title' =>  t('End Date of Profile'),
      'help'  =>  t('The date and time the profile ends'),
      'field' =>  array(
        'handler' => 'views_handler_field_datetime', 
        'click sortable' => TRUE,
        ),  
      'sort' => array(
        'handler' => 'views_handler_sort_datetime',
        ),
      'filter' => array(
        'handler' => 'views_handler_filter_datetime',
        ),  
      'argument' => array(
        'handler' => 'views_handler_argument_date',
        ),  
      );  */
  return $data;
}
