<?php


require_once ( dirname(__FILE__) . '/sensors_chart.helper.inc');

/** 
 * implementation of hook_block_view()
 * see 
 * http://drupal.stackexchange.com/questions/46227/creating-multiple-blocks-programmatically
 for details of multiple blocks
 */
function sensors_chart_block_view($delta = '') {
  $block = array();

  foreach(sensors_chart_get_auto_blocks_db() as $blocks_auto) {
    switch($delta) {
      case $blocks_auto->chartID . '_' . str_replace(' ','_', $blocks_auto->title):
        $block = array(
            'subject' => t(''),
            'content' => sensors_chart_gen_chart($blocks_auto->chartID),
            );
        break;
     case 'current_reading_chart':
        $block = array(
            'subject' => t($blocks_auto->title),
            'content' => current_reading_chart_block(),
            );
        break;
      case 'last24h_reading_chart':
        $block = array(
            'subject' => t(''),
            'content' => last24h_reading_chart_block(),
            );
        break;


    }
  }
  return $block;
}
/* 
 * Function to return a list of chart_axis
 */
function sensors_chart_axis_list_block($axisID  = NULL, $chartID = NULL, $select = NULL, $link_url = NULL) {

  $block = array();
  $axis_db = sensors_chart_get_charts_axis_db($axisID, $chartID);
  if($link_url == NULL) 
    $link_url = 'admin/config/sensors_chart';


  $header = array(
      'axisID' => t('Axis ID'),
      'sensorID' => t('Sensor ID'),
      'title' => t('Chart Title'),
      'dataset_type' => t('Dataset Type'),
      'colour' => t('Dataset Colour'),
      'range' => t('Range'),
      );

  $rows=array();
  if($axis_db != NULL) { 
    foreach($axis_db as $axis) {
      $row=array();
      $row['axisID'] = '<a href="?q=' . $link_url . '&chartID=' . $axis->chartID 
        . '&axisID=' . $axis->axisID . '&sensorID=' . $axis->sensorID .'">' 
        . $axis->axisID . '</a>' ;
      $row['sensorID'] = '<a href="?q=' . $link_url . '&chartID=' . $axis->chartID 
        . '&axisID=' . $axis->axisID . '&sensorID=' . $axis->sensorID .'">' 
        . $axis->sensorID . '</a>' ;
      $row['title'] = '<a href="?q=' . $link_url . '&chartID=' . $axis->chartID 
        . '&axisID=' . $axis->axisID . '&sensorID=' . $axis->sensorID .'">' 
        .($axis->title!=NULL 
            ? $axis->title 
            : $axis->sensorID . ':' . $axis->name . ' (' . htmlspecialchars_decode($axis->units) . ')' ) . '</a>' ;
            $row['dataset_type'] = '<a href="?q=' . $link_url . '&chartID=' . $axis->chartID 
            . '&axisID=' . $axis->axisID . '&sensorID=' . $axis->sensorID .'">' 
            . sensors_chart_get_chart_datasets_list($axis->dataset_type) . '</a>' ;
            $row['colour'] = '<a style="color: ' . $axis->colour . ';" href="?q=' . $link_url . '&chartID=' . $axis->chartID 
            . '&axisID=' . $axis->axisID . '&sensorID=' . $axis->sensorID .'">' 
            . $axis->colour . '</a>' ;
            $row['range'] = '<a href="?q=' . $link_url . '&chartID=' . $axis->chartID 
            . '&axisID=' . $axis->axisID . '&sensorID=' . $axis->sensorID .'">' 
            .  $axis->min . ' - ' . $axis->max .'</a>' ;
            $rows[$axis->axisID] = $row;
            }
            }
            $block['pager'] = array('#theme' => 'pager');
            if(!isset($select)) {
            $block['sensors_chart_tableselect'] = array(
              '#theme' => 'table',
              '#header' => $header,
              '#rows' => $rows,
              '#empty' => t('No chart are available. <a href="?q=admin/config/sensors_chart_types">Please add a new chart type </a>'),
              );
            } else {
              $block['sensors_chart_tableselect'] = array(
                  '#type' => 'tableselect',
                  '#header' => $header,
                  '#options' => $rows,
                  '#empty' => t('No chart are available. <a href="?q=admin/config/sensors_chart">Please add a new chart </a>'),
                  '#multiple' => FALSE,
                  '#default_value' => $select,
                  );
            }
            if($axisID != NULL) {
              $block['add_axis_link'] = array(
                  '#markup' => '<a href="?q=admin/config/sensors_chart&chartID=' . $chartID 
                  . '">Create New Axis</a>',
                  );
            } else {
              $block['edit_axis_link'] = array(
                  '#markup' => '<p>Select an axis above to edit.</p>',
                  );
            }

            return $block;
}

function sensors_chart_axis_add_block($axisID = NULL,$chartID,$link_url = NULL) {

  $update = FALSE;
  if($axisID != NULL && $chartID != NULL) {
    $form_state['values']['chartID'] = $chartID;
    $form_state['values']['axisID'] = $axisID;
    $update = TRUE;
    $axis = sensors_chart_get_charts_axis_db($axisID);
  }

  if($link_url == NULL) 
    $link_url = 'admin/config/sensors_chart';


  $form['fieldset_create'] = array(
      '#type' => 'fieldset',
      '#prefix' => '<div id="sensors_chart_axis_fieldset">',
      '#suffix' => '</div>',
      '#title' => t('Axes'),
      );
  $form['fieldset_create']['row_container_1'] = array(
      '#markup' => '<div id="sensors_chart_axis_row_1">'
      );
  $form['fieldset_create']['sensor_chart_axis_fieldset'] = array(
      '#type' => 'fieldset',
      '#prefix' => '<div id="sensors_chart_axis_type_fieldset">',
      '#suffix' => '</div>',
      '#title' => t('Available Axis'),
      );
  $form['fieldset_create']['sensor_chart_axis_fieldset']['sensor_chart_axis_list'] = 
    sensors_chart_axis_list_block($axisID, $chartID, NULL,'admin/config/sensors_chart');

  $form['fieldset_create']['sensors_chart_axis_help'] = array(
      '#markup' => '<p>' .
      ($update 
       ? t('Update axis details below, once complete, press \'Add Axis to Chart\'.') 
       : t('Enter details of a new axis for the chart below, once complete press the \'Add Axis to Chart\' button. Alternatively, select an axis above to update and existing axis.') 
      ) . '</p>',
      );

  $form['fieldset_create']['row_container_1_end'] = array(
      '#markup' => '</div>'
      );
  $form['fieldset_create']['row_container_2'] = array(
      '#markup' => '<div id="sensors_row_2">'
      );
  $form['fieldset_create']['sensors_chart_axis_name_desc'] = array(
      '#type' => 'fieldset',
      '#prefix' => '<div id="sensors_chart_axis_type_name_desc">',
      '#suffix' => '</div>',
      );

  if($update) {
    $form['fieldset_create']['sensors_chart_axis_name_desc']['example'] = 
      sensors_chart_gen_chart($chartID, $axisID);

    $form['fieldset_create']['sensors_chart_axis_name_desc']['axisID'] = array(
        '#type' => 'textfield',
        '#title' => t('Axis ID'),
        '#size' => 60, 
        '#maxlength' => 32, 
        '#default_value' => $axis[$axisID]->axisID,
        '#attributes' => array('disabled' => 'TRUE'),
        );
  } 

  $form['fieldset_create']['sensors_chart_axis_name_desc']['sensors_chart_axis_title'] = array(
      '#type' => 'textfield',
      '#title' => t('Axis Title'),
      '#size' => 60, 
      '#maxlength' => 32, 
      '#default_value' => ($update ? $axis[$axisID]->title : NULL ),
      );
  $chart_datasets = sensors_chart_get_chart_datasets_list();
  $form['fieldset_create']['sensors_chart_axis_name_desc']['sensors_chart_dataset_select'] = array(
      '#type' => 'select',
      '#title' => t('Chart Dataset Type'),
      '#maxlength' => 32, 
      '#default_value' => (isset($axis[$axisID]->dataset_type) 
        ? $axis[$axisID]->dataset_type: NULL ),
      '#options' => $chart_datasets,
      '#required' => TRUE,
      );
  $form['fieldset_create']['sensors_chart_axis_name_desc']['sensors_chart_axis_colour'] = array(
      '#type' => 'textfield',
      '#title' => t('Dataset Colour'),
      '#size' => 60, 
      '#maxlength' => 32, 
      '#default_value' => ($update ? $axis[$axisID]->colour : NULL ),
      '#required' => TRUE,
      );
  $form['fieldset_create']['sensors_chart_axis_name_desc']['sensors_chart_axis_min'] = array(
      '#type' => 'textfield',
      '#title' => t('Min Axis Value'),
      '#size' => 60, 
      '#maxlength' => 256, 
      '#default_value' => ($update ? $axis[$axisID]->min : 0 ),
      );
  $form['fieldset_create']['sensors_chart_axis_name_desc']['sensors_chart_axis_max'] = array(
      '#type' => 'textfield',
      '#title' => t('Max Axis Value'),
      '#size' => 60, 
      '#maxlength' => 256, 
      '#default_value' => ($update ? $axis[$axisID]->max : NULL ),
      );
  $form['fieldset_create']['sensors_chart_axis_name_desc']['sensor_select'] = array(
      '#type' => 'select',
      '#title' => t('Assign Axis to Sensor'),
      '#maxlength' => 32, 
      '#default_value' => ($update ? $axis[$axisID]->sensorID: NULL ),
      '#options' => get_sensors_list(), //$sensors_list,
      );

  $form['fieldset_create']['row_container_2_end'] = array(
      '#markup' => '</div>'
      );


  $form['fieldset_create']['submit'] = array(
      '#type' => 'submit',
      '#title' => t('Create Profile'),
      '#value' => ($update ? t('Update Chart Axis') : t('Add Axis to Chart')),
      '#prefix' => '<div class="sensors_submit">',
      '#suffix' => '</div>',
      '#submit' => array('sensors_chart_axis_add_submit'),
      '#required' => TRUE,
      );

  return $form;
}

function sensors_chart_axis_add_submit($form, &$form_state) {
  if(!( isset($form_state['values']['sensor_select']) && 
        isset($form_state['values']['chartID']) &&
        isset($form_state['values']['sensors_chart_axis_colour']) &&
        isset($form_state['values']['sensors_chart_dataset_select']) &&
        isset($form_state['values']['sensors_chart_axis_min']))) {
    drupal_set_message("You must specify at least 'Sensor', 'Colour',"
        . " and 'Min  Axis Value' to add or update an axis .", 'warning');
  } else {
    if(isset($form_state['values']['chartID']) && isset($form_state['values']['axisID'])) {
      /* UPDATE existing Device */
      $axisID = $form_state['values']['axisID'];
      $tmp = sensors_chart_update_axis_db(
          $form_state['values']['axisID'],
          $form_state['values']['chartID'],
          $form_state['values']['sensor_select'],
          (isset($form_state['values']['sensors_chart_axis_title']) 
           ? $form_state['values']['sensors_chart_axis_title'] : NULL ),
          $form_state['values']['sensors_chart_dataset_select'],
          $form_state['values']['sensors_chart_axis_colour'],
          (isset($form_state['values']['sensors_chart_axis_max']) 
           ? $form_state['values']['sensors_chart_axis_max'] : NULL ),
          $form_state['values']['sensors_chart_axis_min']);
      if($tmp < 0) {
        drupal_set_message(t('Your axis could not be updated...'), 'warning');
      } else {
        drupal_set_message(t('Your axis has been updated.'), 'status');
      }
    } else {
      $axisID = 0;

      $axisID = sensors_chart_insert_axis_db(
          $form_state['values']['chartID'],
          $form_state['values']['sensor_select'],
          (isset($form_state['values']['sensors_chart_axis_title']) 
           ? $form_state['values']['sensors_chart_axis_title'] : NULL ),
          $form_state['values']['sensors_chart_dataset_select'],
          $form_state['values']['sensors_chart_axis_colour'],
          (isset($form_state['values']['sensors_chart_axis_max']) 
           ? $form_state['values']['sensors_chart_axis_max'] : NULL ),
          $form_state['values']['sensors_chart_axis_min']);
      if($axisID < 0) {
        drupal_set_message(t('New axis could not be created...'), 'warning');
      } else {
        drupal_set_message(t('Your new axis has been created.'), 'status');
      }
    }
    $form_state['redirect'] = array(
        'admin/config/sensors_chart',
        array(
          'query' => array(
            'chartID' => $form_state['values']['chartID'],
            ),
          ),	
        );
  }
}
/* 
 * Function to return a list of chart_sensor relationships
 */
function sensors_chart_list_block($chartID = NULL,$select = NULL, $link_url = NULL) {

  $block = array();
  $chart_db = sensors_chart_get_charts_db($chartID);

  if($link_url == NULL) 
    $link_url = 'admin/config/sensors_chart';


  $header = array(
      'chartID' => t('Chart ID'),
      'sensorID' => t('Sensor ID'),
      'title' => t('Chart Title'),
      'description' => t('Chart Description'),
      'width_height' => t('Chart Width X Height'),
      'priority' => t('Priority'),
      'typeID' => t('Type ID'),
      'name' => t('Chart Type Name'),
      't.description' => t('Type Description'),
      'type' => t('Chart Type'),
      'plugin' => t('Plugin Used'),
      );

  $rows=array();
  if($chart_db != NULL) { 
    foreach($chart_db as $chart) {
      $chart_type = sensors_chart_get_types($chart->typeID);
      $row=array();
      $row['chartID'] = '<a href="?q=' . $link_url . '&chartID=' 
        . $chart->chartID . '">' . $chart->chartID . '</a>' ;
      $row['sensorID'] = '<a href="?q=' . $link_url . '&chartID=' 
        . $chart->chartID . '">' . $chart->sensorID . '</a>' ;
      $row['chart_title'] = '<a href="?q=' . $link_url . '&chartID=' 
        . $chart->chartID . '">' . $chart->title . '</a>' ;
      $row['description'] = '<a href="?q=' . $link_url . '&chartID=' 
        . $chart->chartID . '">' . $chart->description . '</a>' ;
      $row['width_height'] = '<a href="?q=' . $link_url . '&chartID=' 
        . $chart->chartID . '">' . $chart->width . ' X ' . $chart->height . '</a>' ;
      $row['priority'] = '<a href="?q=' . $link_url . '&chartID=' 
        . $chart->chartID . '">' . $chart->priority . '</a>' ;
      $row['typeID'] = '<a href="?q=' . $link_url . '&chartID=' 
        . $chart->chartID . '">' . $chart->typeID . '</a>' ;
      $row['name'] = '<a href="?q=' . $link_url . '&chartID=' 
        . $chart->chartID . '">' . $chart_type['name'] . '</a>' ;
      $row['t.description'] = '<a href="?q=' . $link_url . '&chartID=' 
        . $chart->chartID . '">' . $chart->description . '</a>' ;
      $row['type'] = '<a href="?q=' . $link_url . '&chartID=' 
        . $chart->chartID . '">' . $chart_type['name'] . '</a>' ;
      $row['plugin'] = '<a href="?q=' . $link_url . '&chartID=' 
        . $chart->chartID . '">' . $chart_type['plugin'] . '</a>' ;
      $rows[$chart->chartID] = $row;
    }
  }
  $block['pager'] = array('#theme' => 'pager');
  if(!isset($select)) {
    $block['sensors_chart_tableselect'] = array(
        '#theme' => 'table',
        '#header' => $header,
        '#rows' => $rows,
        '#empty' => t('No chart are available. <a href="?q=admin/config/sensors_chart_types">Please add a new chart</a>'),
        );
  } else {
    $block['sensors_chart_tableselect'] = array(
        '#type' => 'tableselect',
        '#header' => $header,
        '#options' => $rows,
        '#empty' => t('No chart are available. <a href="?q=admin/config/sensors_chart">Please add a new chart </a>'),
        '#multiple' => FALSE,
        '#default_value' => $select,
        );
  }
  if($chartID != NULL) {
    $block['add_chart_link'] = array(
        '#markup' => '<a href="?q=admin/config/sensors_chart">Create New Chart</a>',
        );
  } else {
    $block['edit_chart_link'] = array(
        '#markup' => '<p>Select a chart above to edit.</p>',
        );
  }
  return $block;
}

/* 
 * Function to return a list of sensor types
 */
function sensors_chart_types_list_block($select = NULL, $link_url = NULL) {

  $block = array();
  $types_db = sensors_chart_get_types();

  if($link_url == NULL) 
    $link_url = 'sensor_types';


  $header = array(
      'typeID' => t('Type ID'),
      'name' => t('Chart Type Name'),
      'description' => t('Type Description'),
      'type' => t('Chart Type'),
      'plugin' => t('Plugin Used'),
      );
  $rows=array();
  if($types_db != NULL) { 
    foreach($types_db as $id => $chart_type) {
      $row=array();
      $row['typeID'] = '<a href="?q=' . $link_url . '&typeID=' 
        . $id . '">' . $id . '</a>' ;
      $row['name'] = '<a href="?q=' . $link_url . '&typeID=' 
        . $id . '">' . $chart_type['name'] . '</a>' ;
      $row['description'] = '<a href="?q=' . $link_url . '&typeID=' 
        . $id . '">' . $chart_type['description'] . '</a>' ;
      $row['type'] = '<a href="?q=' . $link_url . '&typeID=' 
        . $id . '">' . $chart_type['type'] . '</a>' ;
      $row['plugin'] = '<a href="?q=' . $link_url . '&typeID=' 
        . $id . '">' . $chart_type['plugin'] . '</a>' ;
      $rows[$id] = $row;
    }
  }
  $block['pager'] = array('#theme' => 'pager');
  if(!isset($select)) {
    $block['sensors_chart_type_tableselect'] = array(
        '#theme' => 'table',
        '#header' => $header,
        '#rows' => $rows,
        '#empty' => t('No chart types are available.'),
        );
  } else {
    $block['sensors_chart_type_tableselect'] = array(
        '#type' => 'tableselect',
        '#header' => $header,
        '#options' => $rows,
        '#empty' => t('No chart types are available. <a href="?q=admin/config/sensors_chart_types">Please add a new chart type </a>'),
        '#multiple' => FALSE,
        '#default_value' => $select,
        '#required' => TRUE,
        );
  }
  return $block;
}

/*
 * Block to show the previous days period avgs
 */
function sensors_chart_yesterday_avg_block($sensorID) {

  $date = get_day_interval_avgs($sensorID, date('Y-m-d',strtotime('now') - 86400));

  $bar_data['night'][] = round($date['night'],0);
  $bar_data['morning'][] = round($date['morning'],0);
  $bar_data['afternoon'][] = round($date['afternoon'],0);
  $bar_data['evening'][] = round($date['evening'],0);
  $block['energy_yesterday'] = render_avg_groupbar_chart($bar_data, 'Yesterday', 200);
  return $block;
}


function sensors_chart_last24h_reading_chart_block() {
  if(isset($_REQUEST['sensorID'])) {
    $sensorID=$_REQUEST['sensorID'];
  } else {
    $sensorID=3;
  }
  $temps = calc_average_reading_set(sensors_get_last_nhour_temps_db(4,24));
  $powers = calc_average_reading_set(sensors_get_last_nhour_powers_db($sensorID,24));
  $block['temp_chart_last24h'] = render_last_nh_reading(4,24,'Temp',$temps);
  $block['power_chart_last24h'] = render_last_nh_reading($sensorID,24,'Power',$powers);
  $temp_range = sensors_get_last_nhour_maxmin_temp_db($sensorID,24);
  /*
     $block['temp_range'] = array(
     '#markup' =>  '<h4>Max: ' . $temp_range['max']['temp'] . '</h4>' .
     '<h3> ' . $reading['temp'] . '</h4>' .
     '<h4>Min: ' . $temp_range['min']['temp'] . '</h4>',
     );*/
  return($block);
}
/**
 * Function to create chart blocks on the fly
 */
function sensors_chart_create_chart_block($chartID,$title = '', $description = '' ) {
  form_load_include($form_state, 'inc', 'block', 'block.admin');
  $form_state = array();
  $form_state['values'] = array(
      'title' => $title,
      'info' => $description,
      'visibility' => '0',
      'pages' => NULL,
      'custom' => '0',
      // The block isn't visible initially
      'regions' => array(
        'seven' => '-1',
        'stark' => '-1',
        ),
      'body' => array(
        'value' => 'hello, test',
        'format' => 'filtered_html',
        ),
      'content' => sensors_chart_gen_chart($chartID),
      );
  // Submit the form programmatically.
  drupal_form_submit('block_add_block_form', $form_state);
}
