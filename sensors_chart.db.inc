<?php
/* 
 * Fuction to query the database for the sensors_chart module
 */

/* 
 * Fuction to SELECT  sensors_chart_types
 */
function sensors_chart_get_charts_db($chartID = NULL) {

  db_set_active();

  $query = db_select('sensors_chart', 'c');
  $query->fields('c', array('chartID', 'sensorID', 'typeID', 'title',
        'height', 'date', 'end_date', 'width', 'description', 'block','
        priority'));

  if($chartID != NULL) {
      $query->condition('c.chartID', $chartID);
  }
  $result = $query->execute()->fetchAllAssoc('chartID');

  return $result;

}

/* 
 * Fuction to SELECT  sensors_chart_types
 */
function sensors_chart_get_charts_axis_db($axisID = NULL,$chartID = NULL, $sensorID = null) {

  db_set_active();

  $query = db_select('sensors_chart_axis', 'a');
  $query->join('sensors_sensor','s',
      's.sensorID = a.sensorID');
  $query->join('sensors_sensor_type','t',
      't.typeID = s.typeID');
  $query->fields('a', array('axisID', 'chartID','sensorID','title','dataset_type',
        'colour','min', 'max'));
  $query->fields('s', array('name', 'description'));
  $query->fields('t', array('units','type','description'));
  if($axisID != NULL) {
      $query->condition('a.axisID', $axisID);
  }
  if($chartID != NULL) {
      $query->condition('a.chartID', $chartID);
  }
  if($sensorID != NULL) {
      $query->condition('a.sensorID', $sensorID);
  }
      $result = $query->execute()->fetchAllAssoc('axisID');

  return $result;

}

/* 
 * Fuction to SELECT sensors_chart relationships
 */
function sensors_chart_get_chart_types_db($typeID = NULL) {

  db_set_active();

  $query = db_select('sensors_chart_type', 't');
  $query->fields('t', array('typeID','name','description','type','plugin'));
  if($typeID != NULL) {
      $query->condition('t.typeID', $typeID);
      $result = $query->execute()->fetchAssoc();
  } else {
      $result = $query->execute()->fetchAllAssoc('typeID');
  }

  return $result;

}


function sensors_chart_get_auto_blocks_db() {

  /* should probably be split into helper functions 
     at a later date
    */

  db_set_active();

  $query = db_select('sensors_chart', 'c');
  $query->fields('c', array('chartID','title',
        'height','width','description','priority'));
  $query->condition('c.block', 1);
  $result = $query->execute()->fetchAllAssoc('chartID');

  return $result;


}


/*
 * INSERT queries
 */
function sensors_chart_insert_chart_type_db($type, $plugin, $name, $description) {
  $tmp = db_insert('sensors_chart_type')
    ->fields(array(
          'type' => $type,
          'name' => $name,	
          'description' => $description,
          'plugin' => $plugin,
          ))
    ->execute();
  return($tmp);
}

function sensors_chart_insert_chart_db($typeID, $chart_title, $description,
    $date, $end_date, $width, $height, $block, $priority) {
  $tmp = db_insert('sensors_chart')
    ->fields(array(
          'sensorID' => 0,
          'typeID' => $typeID,
          'title' => $chart_title,	
          'description' => $description,
          'date' => $date,
          'end_date' => $end_date,
          'width' => $width,
          'height' => $height,
          'block' => $block,
          'priority' => $priority,
          ))
    ->execute();
  return($tmp);
}

function sensors_chart_insert_axis_db($chartID, $sensorID, $title, $dataset_type, $colour, $max, $min) {
  $tmp = db_insert('sensors_chart_axis')
    ->fields(array(
          'chartID' => $chartID,
          'sensorID' => $sensorID,
          'title' => (empty($title) ? NULL : $title),
          'dataset_type' => $dataset_type,
          'colour' => $colour,
          'min' => $min,
          'max' => (empty($max) ? NULL : $max),
          ))
    ->execute();
  return($tmp);
}
/*
 * UPDATE queries 
 */

function sensors_chart_update_chart_db($chartID, $typeID, $chart_title, 
    $description, $date, $end_date, $width, $height, $block, $priority) {
  $tmp = db_update('sensors_chart')
    ->fields(array(
          'typeID' => $typeID,
          'title' => $chart_title,	
          'description' => $description,
          'date' => $date,
          'end_date' => $end_date,
          'width' => $width,
          'height' => $height,
          'block' => $block,
          'priority' => $priority,
          ))
    ->condition('chartID', $chartID)
    ->execute();
  return($tmp);
}

function sensors_chart_update_chart_type_db($typeID, $type, $plugin, $name, $description) {
  $tmp = db_update('sensors_chart_type')
    ->fields(array(
          'type' => $type,	
          'name' => $name,
          'description' => $description,
          'plugin' => $plugin,
          ))
    ->condition('typeID', $typeID)
    ->execute();
  return($tmp);
}

function sensors_chart_update_axis_db($axisID,$chartID, $sensorID, $title, $dataset_type, $colour, $max, $min) {
  $tmp = db_update('sensors_chart_axis')
    ->fields(array(
          'chartID' => $chartID,
          'sensorID' => $sensorID,
          'title' => (empty($title) ? NULL : $title),	
          'dataset_type' => $dataset_type,
          'colour' => $colour,
          'min' => $min,
          'max' => (empty($max) ? NULL : $max),
          ))
    ->condition('axisID', $axisID)
    ->execute();
  return($tmp);
}
